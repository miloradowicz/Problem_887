
#include <iostream>

unsigned int log2(unsigned long long x)
{
  unsigned int i = 0;
  while ((x >>= 1) != 0)
    i++;
  return i;
}

//unsigned long long S(unsigned int n)
//{
//  unsigned long long i, j = 0;
//  if (n == 0 || n == 1 || n == 2)
//    return n;
//  i = S(n - 1);
//  j = S(n - 2);
//  if ((n & 1) == 1)
//    return (i << 1) + i - (j << 1);
//  else
//    return (i << 1) - j;
//}

unsigned long long S(unsigned int n)
{
  if (n == 0 || n == 1 || n == 2)
    return n;
  unsigned int i = n - 1 >> 1;
  unsigned long long r = 1ull << i;
  if ((n & 1) == 1)
    return (r << 1) + r - 2;
  else
    return (r << 2) - 2;
}

unsigned long long R(unsigned long long x, unsigned int n)
{
  unsigned long long r = 0;
  for (unsigned int i = 0; i < n; i++)
  {
    r <<= 1;
    if ((x & 1) == 1)
      r |= 1;
    x >>= 1;
  }
  return r;
}

unsigned long long M(unsigned long long x, unsigned int n)
{
  unsigned long long r = 0, m = 1;
  for (unsigned int i = 0; i < n; i++)
  {
    r |= x & (m << i);
  }
  return r;
}

int main()
{
  unsigned long long N, res = 0;
  std::cin >> N;
  unsigned int m, n;
  n = log2(N) + 1;
  m = n - (n >> 1);
  res += S(n - 1);
  res += M(N >> (n >> 1), m - 1) + 1;
  if (R((N >> m), (n >> 1)) > M(N, (n >> 1)))
    --res;
  std::cout << res;
}